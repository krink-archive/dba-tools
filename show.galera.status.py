#!/bin/env python

__version__ = 'show.galera.status.0.0.2'

dbconf  = '/etc/db.conf'

with open(dbconf) as conf:
  for line in conf:
    if line.startswith("define('dbaUser'"):
      dbaUser = line.split(',')[1].strip('\'').split('\'')[0]
    if line.startswith("define('dbaPass'"):
      dbaPass = line.split(',')[1].strip('\'').split('\'')[0]

config = {
  'user': dbaUser,
  'password': dbaPass,
  'unix_socket': '/var/lib/mysql/mysql.sock',
  'database': 'mysql',
  'raise_on_warnings': True,
}

import sys
try:
  import mysql.connector
except ImportError as e:
  print e
  print 'yum install mysql-connector-python'
  sys.exit(1)

try:
  cnx = mysql.connector.connect(**config)
except mysql.connector.Error as e:
  print e
  sys.exit(1)

cursor = cnx.cursor(buffered=True)
try:
  sql = "show status;"
  cursor.execute(sql)
  rows = cursor.fetchall()

except mysql.connector.Error as e:
  print e
  sys.exit(1)

cursor.close()
cnx.close()


for row in rows:
    #print str(row[0]), str(row[1])

    if str(row[0]) == 'wsrep_cluster_size':
        print str(row[0]), str(row[1])

    if str(row[0]) == 'wsrep_cluster_status':
        print str(row[0]), str(row[1])

    if str(row[0]) == 'wsrep_connected':
        print str(row[0]), str(row[1])

    if str(row[0]) == 'wsrep_evs_state':
        print str(row[0]), str(row[1])

    if str(row[0]) == 'wsrep_incoming_addresses':
        print str(row[0]), str(row[1])

    if str(row[0]) == 'wsrep_local_state':
        print str(row[0]), str(row[1])

    if str(row[0]) == 'wsrep_local_state_comment':
        print str(row[0]), str(row[1])

    if str(row[0]) == 'wsrep_ready':
        print str(row[0]), str(row[1])


sys.exit(0)
