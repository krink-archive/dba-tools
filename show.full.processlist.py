#!/bin/env python

__version__ = 'show.full.processlist.0.0.1'

dbconf  = '/etc/db.conf'

with open(dbconf) as conf:
  for line in conf:
    if line.startswith("define('dbaUser'"):
      dbaUser = line.split(',')[1].strip('\'').split('\'')[0]
    if line.startswith("define('dbaPass'"):
      dbaPass = line.split(',')[1].strip('\'').split('\'')[0]

config = {
  'user': dbaUser,
  'password': dbaPass,
  'unix_socket': '/var/lib/mysql/mysql.sock',
  'database': 'mysql',
  'raise_on_warnings': True,
}

import sys
try:
  import mysql.connector
except ImportError as e:
  print e
  print 'yum install mysql-connector-python'
  sys.exit(1)

try:
  cnx = mysql.connector.connect(**config)
except mysql.connector.Error as e:
  print e
  sys.exit(1)

cursor = cnx.cursor(buffered=True)
try:
  sql = "show full processlist;"
  cursor.execute(sql)
  column_names = [i[0] for i in cursor.description]
  rows = cursor.fetchall()

except mysql.connector.Error as e:
  print e
  sys.exit(1)

cursor.close()
cnx.close()

print column_names
for row in rows:
    print row

sys.exit(0)
