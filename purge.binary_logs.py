#!/bin/env python

#mysql> set global expire_logs_days=7; flush logs;

__version__ = '0.0.1'

dbconf  = '/etc/db.conf'

with open(dbconf) as conf:
  for line in conf:
    if line.startswith("define('dbaUser'"):
      dbaUser = line.split(',')[1].strip('\'').split('\'')[0]
    if line.startswith("define('dbaPass'"):
      dbaPass = line.split(',')[1].strip('\'').split('\'')[0]

config = {
  'user': dbaUser,
  'password': dbaPass,
  #'host': 'localhost',
  'unix_socket': '/var/lib/mysql/mysql.sock',
  'database': 'mysql',
  'raise_on_warnings': True,
}


import sys
try:
  import mysql.connector
except ImportError as e:
  print e
  print 'yum install mysql-connector-python'
  sys.exit(1)


try:
  cnx = mysql.connector.connect(**config)
except mysql.connector.Error as e:
  print e
  sys.exit(1)

cursor = cnx.cursor()
try:
  sql = "show binary logs;"
  cursor.execute(sql)
  rows = cursor.fetchall()
  keeprow = str(rows[-2][0])
  lastrow = str(rows[-1][0]) 

  if keeprow:
    sql = "purge binary logs to '" + keeprow + "';"
    print str(sql)
    cursor.execute(sql)
    cnx.commit()
  else:
    print "no keeprow"

except mysql.connector.Error as e:
  print e
  sys.exit(1)

cursor.close()
cnx.close()


sys.exit(0)
