#!/usr/bin/env bash

conf="/etc/db.conf"
#define('dbaUser','user');
#define('dbaPass','pass');

dbaUser=$(grep ^define $conf | grep dbaUser | awk -F"," '{print $2}' | awk -F"'" '{print $2}')
dbaPass=$(grep ^define $conf | grep dbaPass | awk -F"," '{print $2}' | awk -F"'" '{print $2}')
dbaServer="localhost"

mysql -u$dbaUser -p$dbaPass -h $dbaServer

#  -P, --port=#        Port number to use for connection or 0 for default to, in
#                      order of preference, my.cnf, $MYSQL_TCP_PORT,
#                      /etc/services, built-in default (3306).

