#!/bin/env python

__version__ = 'show.clients.processlist.0.0.1'

dbconf  = '/etc/db.conf'

with open(dbconf) as conf:
  for line in conf:
    if line.startswith("define('dbaUser'"):
      dbaUser = line.split(',')[1].strip('\'').split('\'')[0]
    if line.startswith("define('dbaPass'"):
      dbaPass = line.split(',')[1].strip('\'').split('\'')[0]

config = {
  'user': dbaUser,
  'password': dbaPass,
  'unix_socket': '/var/lib/mysql/mysql.sock',
  'database': 'mysql',
  'raise_on_warnings': True,
}

import sys
try:
  import mysql.connector
except ImportError as e:
  print e
  print 'yum install mysql-connector-python'
  sys.exit(1)

try:
  cnx = mysql.connector.connect(**config)
except mysql.connector.Error as e:
  print e
  sys.exit(1)

cursor = cnx.cursor(buffered=True)
try:
  sql = "show full processlist;"
  cursor.execute(sql)
  rows = cursor.fetchall()

except mysql.connector.Error as e:
  print e
  sys.exit(1)

cursor.close()
cnx.close()

#[u'Id', u'User', u'Host', u'db', u'Command', u'Time', u'State', u'Info']
for row in rows:
    #print Id, User, Host, Db, Command, State, Info

    #print row[7]
    #'show full processlist'
    if row[7] == 'show full processlist':
        continue
    #    print "Yep " + str(row[7])
    #else:
    #    print "Nope" + str(row[7])

    if row[3]:
        #print row
        print str(row[0]),str(row[1]),str(row[3]),str(row[4]),str(row[5]),str(row[6])

sys.exit(0)
