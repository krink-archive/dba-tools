#!/usr/bin/env bash

conf="/etc/db.conf"
#define('dbaUser','user');
#define('dbaPass','pass');

dbaUser=$(grep ^define $conf | grep dbaUser | awk -F"," '{print $2}' | awk -F"'" '{print $2}')
dbaPass=$(grep ^define $conf | grep dbaPass | awk -F"," '{print $2}' | awk -F"'" '{print $2}')
dbaServer="localhost"

SQL="SELECT TABLE_SCHEMA as DbName ,TABLE_NAME as TableName ,ENGINE as Engine FROM information_schema.TABLES WHERE ENGINE='MyISAM' AND TABLE_SCHEMA NOT IN('mysql','information_schema','performance_schema');"
mysql -u$dbaUser -p$dbaPass -h $dbaServer -N -e "$SQL"

#  -P, --port=#        Port number to use for connection or 0 for default to, in
#                      order of preference, my.cnf, $MYSQL_TCP_PORT,
#                      /etc/services, built-in default (3306).

