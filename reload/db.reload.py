#!/usr/bin/python

__version__ = '0.0.0.3'

import sys
sys.dont_write_bytecode = True

try:
  import config
except ImportError as e:
  print e
  config_example = """
this = dict(
  dbaUser  = 'krink',
  dbaPass  = 'XXXXXXXX',
  replUser = 'rdbc4-ma1-01',
  replPass = 'XXXXXXXX',
  replSrc  = '192.168.193.43',
  replDest = '192.168.193.45',
  replDestPort = '3307',
)
"""
  print config_example
  open('config.py', 'w').write(config_example)
  print 'Wrote config.py'
  sys.exit(99)

import subprocess
import time

ssh = 'ssh -t -o LogLevel=Error -o ConnectTimeout=8 '

def sshCmdStdOut(host=None,cmd=None):
    """ sys stdout
            provides real time subprocess """
    cmdline = str(ssh) + ' ' + str(host) + ' ' + str(cmd)
    subcmd = subprocess.Popen(cmdline.split(), stdout=subprocess.PIPE)
    exitcode = subcmd.wait()
    if (exitcode != 0):
        print 'Error exitcode ' + str(exitcode)
    for line in subcmd.stdout:
        sys.stdout.write(line)
        sys.stdout.flush()
    return exitcode

def sshCmdDict(host=None,cmd=None):
    """ dict
            return dict """
    cmdline = str(ssh) + ' ' + str(host) + ' ' + str(cmd)
    subcmd = subprocess.Popen(cmdline.split(), stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    output, err = subcmd.communicate()
    returnDict = {}
    exitcode = subcmd.wait()
    if (exitcode != 0):
        returnDict[host] = str(exitcode) + ' ' + str(err)
        return returnDict, exitcode
    returnDict[host] = output
    return returnDict, exitcode

def runCmd(host=None, cmdline=None):
    subcmd = subprocess.Popen(cmdline.split(), stdout=subprocess.PIPE)
    output = subcmd.communicate()
    exitcode = subcmd.wait()
    if (exitcode != 0):
         print 'Error:  ' + str(exitcode) + ' ' + str(host) + ' ' + str(output)
    return ''.join(output[0])

def check_master_position_file(host=None, file=None):
    cmd = 'head -1 ' + file
    cmdline = str(ssh) + str(host) + ' ' + str(cmd)
    line = runCmd(host, cmdline)

    if line.startswith('-- CHANGE MASTER TO MASTER_LOG_FILE='):
        print 'Success ' + str(line)
        return line
    else:
        print "Nope!"
        return False

def grant_replication():
    #GRANT REPLICATION SLAVE ON *.* TO 'sdbc4-ma1-01'@'192.168.193.44' identified by 'XXXXXXXXX';
    sql  = "GRANT REPLICATION SLAVE ON *.* TO '"
    sql += str(replUser) + "'@'" + str(replDest) + "' identified by '"
    sql += str(replPass) + "';"
    cmd  = '/usr/bin/mysql -u' + str(dbaUser) + ' -p' + str(dbaPass) + ' '
    cmd += ' -e "' + str(sql) + '" '
    #print 'ran: ' + str(cmd)
    grant = sshCmdStdOut(replSrc, cmd)
    #print str(grant)
    print 'grant replication sql commands on Master: ' + str(replSrc)
    return grant

def prep_slave(host=None):
    #/usr/bin/mysql -ukrink -pXXXXXXXX  -e "SHOW ALL SLAVES STATUS \G;" | grep Connection_name | awk '{print $2}'
    print 'prep_slave HOST: ' + str(host)
    Connection_name = None
    sql = 'SHOW ALL SLAVES STATUS \G;'
    cmd = '/usr/bin/mysql -u' + str(dbaUser) + ' -p' + str(dbaPass) + ' '
    cmd += ' -e "' + str(sql) + '" '
    cmd += "| grep Connection_name | awk '{print $2}'"
    ret, e = sshCmdDict(host, cmd)
    for key in sorted(ret.iterkeys()):
        #print key
        #print ret[key]
        #Connection_name = str(ret[key])
        Connection_name = str(ret[key])
        print 'Slave host: ' + str(host) + ' is slave_running Connection_name: ' + str(Connection_name)

    if Connection_name:
        Connection_name = Connection_name.strip()
        #print 'CONNECTION_NAME: ' + str(Connection_name)
        sql = "STOP SLAVE '" + str(Connection_name) + "';"
        cmd = '/usr/bin/mysql -u' + str(dbaUser) + ' -p' + str(dbaPass) + ' '
        cmd += ' -e "' + str(sql) + '" '
        #print 'host: ' + str(host) + ' cmd: ' + str(cmd)
        stop_slave = sshCmdStdOut(host, cmd)
        print 'stop_slave ' + str(stop_slave)

        sql = "RESET SLAVE '" + str(Connection_name) + "' ALL;"
        cmd = '/usr/bin/mysql -u' + str(dbaUser) + ' -p' + str(dbaPass) + ' '
        cmd += ' -e "' + str(sql) + '" '
        reset_slave = sshCmdStdOut(host, cmd)
        print 'reset_slave ' + str(reset_slave)

    return True

if __name__ == "__main__":

    dbaUser =  config.this['dbaUser']
    dbaPass =  config.this['dbaPass']
    replUser = config.this['replUser']
    replPass = config.this['replPass']
    replSrc =  config.this['replSrc']
    replDest = config.this['replDest']
    replDestPort = config.this['replDestPort']

    masterfile = '/tmp/MASTER.POSITON.' + str(replSrc) + '-' + str(replDest)

    #grant replication (replSrc)
    grant = grant_replication()
    print str(grant)

    #prep slave (replDest)
    prep = prep_slave(replDest)
    print str(prep)
    #sys.exit(100)

    #launch dump_pipe
    cmd = 'ssh -o LogLevel=Error -T ' + str(replSrc) + ' '
    cmd += '"bash -c \'/usr/bin/nohup /usr/bin/mysqldump --socket=/var/lib/mysql/mysql.sock -u' + str(dbaUser) + ' -p' + str(dbaPass)
    cmd += ' --master-data=2 --single-transaction --quick --all-databases  '
    cmd += ' |  /usr/bin/tee >(/bin/grep "MASTER_LOG_FILE=" >' + masterfile + ') '
    cmd += ' | /usr/bin/mysql -u' + str(dbaUser) + ' -p' + str(dbaPass)
    cmd += ' -h ' + str(replDest) + ' -P ' + str(replDestPort) + ' >>' + str(masterfile) + '.error.out  2>&1 &\'"'
    runcmd = subprocess.call(cmd, shell=True)
    print 'Launched mysqldump on ' + str(replSrc)

    #check master position file
    while check_master_position_file(replSrc, masterfile) is False:
        print 'sleep 3...'
        sys.stdout.flush()
        time.sleep(3)

    #dump_pipe done, we have master position file
    mp = check_master_position_file(replSrc, masterfile)
    mpList = mp.split()
    MASTER_LOG_FILE = mpList[4]
    MASTER_LOG_POS  = mpList[5]

    #CHANGE MASTER 'db-ca4-01' TO MASTER_HOST='192.168.247.21', MASTER_USER='dbc2-ca4-01', MASTER_PASSWORD='XXXXXXXXXX', MASTER_LOG_FILE='db-ca4-01.002647', MASTER_LOG_POS=39435046;

    CHANGEMASTERSTATEMENT  = "CHANGE MASTER '" + str(replSrc) + "' TO MASTER_HOST='" + str(replSrc) + "', "
    CHANGEMASTERSTATEMENT += "MASTER_USER='" + str(replUser) + "', MASTER_PASSWORD='" + str(replPass) + "', "
    CHANGEMASTERSTATEMENT += str(MASTER_LOG_FILE) + str(MASTER_LOG_POS)

    #cmd = "/bin/echo hello1 >/tmp/1"
    #runcmd = runCmd(replDest, cmd)
    #print str(runcmd)

    cmd = 'echo "' + str(CHANGEMASTERSTATEMENT) + '" >' + str(masterfile)
    #cmdline = 'ssh -t -o LogLevel=Error ' + str(replDest) + ' ' + str(cmd)
    run = sshCmdStdOut(replDest, cmd)
    print str(run)
    print 'wrote ' + str(masterfile) + ' ' + str(replDest)

    cmd = '/usr/bin/mysql -u' + str(dbaUser) + ' -p' + str(dbaPass) + ' < ' + str(masterfile)
    #cmdline = 'ssh -t -o LogLevel=Error ' + str(replDest) + ' ' + str(cmd)
    run = sshCmdStdOut(replDest, cmd)
    print str(run)
    print 'change master to ' + str(masterfile) + ' ' + str(replDest)

    sql = "START SLAVE '" + str(replSrc) + "';"
    cmd = 'echo "' + str(sql) + '" | /usr/bin/mysql -u' + str(dbaUser) + ' -p' + str(dbaPass)
    #cmdline = 'ssh -t -o LogLevel=Error ' + str(replDest) + ' ' + str(cmd)
    run = sshCmdStdOut(replDest, cmd)
    print str(run)
    print 'start slave ' + str(replDest)


    print "done"

