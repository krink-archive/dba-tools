#!/bin/env python

__version__ = 'show.running.processlist.0.0.1'

dbconf  = '/etc/db.conf'

with open(dbconf) as conf:
  for line in conf:
    if line.startswith("define('dbaUser'"):
      dbaUser = line.split(',')[1].strip('\'').split('\'')[0]
    if line.startswith("define('dbaPass'"):
      dbaPass = line.split(',')[1].strip('\'').split('\'')[0]

config = {
  'user': dbaUser,
  'password': dbaPass,
  'unix_socket': '/var/lib/mysql/mysql.sock',
  'database': 'mysql',
  'raise_on_warnings': True,
}

import sys
try:
  import mysql.connector
except ImportError as e:
  print e
  print 'yum install mysql-connector-python'
  sys.exit(1)

try:
  cnx = mysql.connector.connect(**config)
except mysql.connector.Error as e:
  print e
  sys.exit(1)

cursor = cnx.cursor(buffered=True)
try:
  sql = "show full processlist;"
  cursor.execute(sql)
  rows = cursor.fetchall()

except mysql.connector.Error as e:
  print e
  sys.exit(1)

cursor.close()
cnx.close()

#[u'Id', u'User', u'Host', u'db', u'Command', u'Time', u'State', u'Info']
for row in rows:

    Id = str(row[0])
    User = str(row[1])
    Host = str(row[2])
    Db = str(row[3])
    Command = str(row[4])
    State = str(row[5])
    Info = str(row[6])
    
    if str(row[3]) == '':
        pass
    if str(row[3]) == 'None':
        pass
    if Info == 'show full processlist':
        pass

    print str(row)
    #print Id, User, Host, Db, Command, State, Info

sys.exit(0)
