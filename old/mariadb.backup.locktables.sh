#!/bin/sh

MYSQL_USER="root"
MYSQL_PASS=""

MYSQL_CONN="-u${MYSQL_USER} -p${MYSQL_PASS}"

if [ $# -ge 1 ]; then
  DBLIST="$@"
else
  # Collect all database names except for
  # mysql, information_schema, and performance_schema
  SQL="SELECT schema_name FROM information_schema.schemata WHERE schema_name NOT IN"
  SQL="${SQL} ('mysql','information_schema','performance_schema')"
  DBLIST=$(mysql ${MYSQL_CONN} -ANe"${SQL}")
fi
echo $DBLIST

echo "FLUSH TABLES WITH READ LOCK" | mysql ${MYSQL_CONN}

MASTERSTATUS=$(echo "SHOW MASTER STATUS" | mysql -N ${MYSQL_CONN})
BINLOG=$(echo $MASTERSTATUS | awk '{print $1}')
BINLOG_POS=$(echo $MASTERSTATUS | awk '{print $2}')

GTID_POS=$(echo "SELECT BINLOG_GTID_POS('${BINLOG}', ${BINLOG_POS})" | mysql -N ${MYSQL_CONN})
echo $GTID_POS


MYSQLDUMP_OPTIONS="--routines --triggers --single-transaction"
mysqldump ${MYSQL_CONN} ${MYSQLDUMP_OPTIONS} --databases ${DBLIST} > mysqldump.mariadb.${BINLOG}.${BINLOG_POS}.${GTID_POS}.sql

echo "UNLOCK TABLES" | mysql ${MYSQL_CONN}

exit 0
