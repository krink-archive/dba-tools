

time=$(date +%F@%H:%M)
directory=$(basename $(pwd))

echo "fio sequential standard output=$directory.$name.txt $directory"

name=4kseqwrite
fio --output=$directory.$name.txt --filename=fio.tmp.file --name=$directory.$name.$time --rw=write --size=1g --bs=4k --direct=1 --refill_buffers --norandommap --randrepeat=0 --ioengine=libaio --rwmixread=100 --iodepth=16 --numjobs=16 --runtime=60 --group_reporting


name=4kseqread
fio --output=$directory.$name.txt --filename=fio.tmp.file --name=$directory.$name.$time --rw=read --size=1g --bs=4k --direct=1 --refill_buffers --norandommap --randrepeat=0 --ioengine=libaio --rwmixread=100 --iodepth=16 --numjobs=16 --runtime=60 --group_reporting



