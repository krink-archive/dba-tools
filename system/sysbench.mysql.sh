#!/bin/bash

if [ $# -lt 1 ]; then
  echo "Usage: $0 prepare|run" && exit 1
fi

conf="/etc/db.conf"
user=$(grep ^define $conf | grep dbaUser | awk -F"," '{print $2}' | awk -F"'" '{print $2}')
pass=$(grep ^define $conf | grep dbaPass | awk -F"," '{print $2}' | awk -F"'" '{print $2}')

db=sysbench

#mysql -u$user -p$pass -e "create database $db;"

#INSERT-only test oltp_insert.lua
sysbench --db-driver=mysql --mysql-user="${user}" --mysql-password="${pass}" \
 --mysql-socket=/var/lib/mysql/mysql.sock --mysql-db="${db}" --range_size=100 \
 --table_size=10000 --tables=2 --threads=1 --events=0 --time=60 \
 --rand-type=uniform /usr/share/sysbench/oltp_insert.lua \
 $1

#mysql -u$user -p$pass -e "drop database $db;"

exit 0
