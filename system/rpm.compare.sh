#!/bin/sh

if [ $# -lt 1 ]; then
  echo "Usage: $0 remote_host" && exit 1
else
  remote="$1"
fi

file1=/tmp/rpms.local.$$
file2=/tmp/rpms.remote.$$

rpm -qa --qf "%{NAME}\n" | sort >$file1
ssh $remote "rpm -qa --qf \"%{NAME}\n\"" | sort >$file2

for line in $(cat $file2)
do
  grep $line $file1 >/dev/null 2>&1 || echo "$line is missing on localhost"
done

for line in $(cat $file1)
do
  grep $line $file2 >/dev/null 2>&1 || echo "$line is missing on remote"
done

rm $file1 $file2

exit 0
