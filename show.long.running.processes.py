#!/bin/env python

__version__ = 'show.clients.processlist.0.0.1'

dbconf  = '/etc/db.conf'

import sys
if sys.argv[1:]:
  runtime = sys.argv[1]
else:
  runtime = 1

with open(dbconf) as conf:
  for line in conf:
    if line.startswith("define('dbaUser'"):
      dbaUser = line.split(',')[1].strip('\'').split('\'')[0]
    if line.startswith("define('dbaPass'"):
      dbaPass = line.split(',')[1].strip('\'').split('\'')[0]

config = {
  'user': dbaUser,
  'password': dbaPass,
  'unix_socket': '/var/lib/mysql/mysql.sock',
  'database': 'mysql',
  'raise_on_warnings': True,
}

try:
  import mysql.connector
except ImportError as e:
  print e
  print 'yum install mysql-connector-python'
  sys.exit(1)

try:
  cnx = mysql.connector.connect(**config)
except mysql.connector.Error as e:
  print e
  sys.exit(1)

cursor = cnx.cursor(buffered=True)
try:
  sql = "select * from INFORMATION_SCHEMA.PROCESSLIST where db is not null and time > " + str(runtime) + ";"
  cursor.execute(sql)
  rows = cursor.fetchall()

except mysql.connector.Error as e:
  print e
  sys.exit(1)

cursor.close()
cnx.close()

for row in rows:
    #if row[7] == 'show full processlist':
    #    continue
    print row

sys.exit(0)
