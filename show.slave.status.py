#!/bin/env python

__version__ = 'show.slave.status.0.0.2'

dbconf  = '/etc/db.conf'

with open(dbconf) as conf:
  for line in conf:
    if line.startswith("define('dbaUser'"):
      dbaUser = line.split(',')[1].strip('\'').split('\'')[0]
    if line.startswith("define('dbaPass'"):
      dbaPass = line.split(',')[1].strip('\'').split('\'')[0]

config = {
  'user': dbaUser,
  'password': dbaPass,
  'unix_socket': '/var/lib/mysql/mysql.sock',
  'database': 'mysql',
  'raise_on_warnings': True,
}

import sys
try:
  import mysql.connector
except ImportError as e:
  print e
  print 'yum install mysql-connector-python'
  sys.exit(1)

try:
  cnx = mysql.connector.connect(**config)
except mysql.connector.Error as e:
  print e
  sys.exit(1)

cursor = cnx.cursor(buffered=True)
try:
  sql = "select version();"
  cursor.execute(sql)
  select_version = cursor.fetchone()

  if str(select_version[0]).startswith("10"):
      sql = "show all slaves status;"
  else:
      sql = "show slave status;"
  cursor.execute(sql)

  if cursor.rowcount > 0:
      show_status = dict(zip(cursor.column_names, cursor.fetchone()))
  else:
      cursor.close()
      cnx.close()
      sys.exit(0)

except mysql.connector.Error as e:
  print e
  sys.exit(1)

cursor.close()
cnx.close()

#for key, value in show_status.iteritems():
#    print key, value

if 'Connection_name' in show_status:
    print 'Connection_name: ' + str(show_status['Connection_name'])

print 'Master_Host: ' + str(show_status['Master_Host'])

print 'Slave_IO_Running: ' + str(show_status['Slave_IO_Running'])
print 'Slave_SQL_Running: ' + str(show_status['Slave_SQL_Running'])

if str(show_status['Replicate_Do_DB']):
    print 'Replicate_Do_DB: ' + str(show_status['Replicate_Do_DB'])

if str(show_status['Replicate_Ignore_DB']):
    print 'Replicate_Ignore_DB: ' + str(show_status['Replicate_Ignore_DB'])

if str(show_status['Replicate_Do_Table']):
    print 'Replicate_Do_Table: ' + str(show_status['Replicate_Do_Table'])

if str(show_status['Replicate_Ignore_Table']):
    print 'Replicate_Ignore_Table: ' + str(show_status['Replicate_Ignore_Table'])

if str(show_status['Replicate_Wild_Do_Table']):
    print 'Replicate_Wild_Do_Table: ' + str(show_status['Replicate_Wild_Do_Table'])

if str(show_status['Replicate_Wild_Ignore_Table']):
    print 'Replicate_Wild_Ignore_Table: ' + str(show_status['Replicate_Wild_Ignore_Table'])

if str(show_status['Last_IO_Errno']):
    print 'Last_IO_Errno: ' + str(show_status['Last_IO_Errno'])

if str(show_status['Last_IO_Error']):
    print 'Last_IO_Error: ' + str(show_status['Last_IO_Errno'])

if str(show_status['Last_SQL_Errno']):
    print 'Last_IO_Error: ' + str(show_status['Last_SQL_Errno'])

if str(show_status['Last_SQL_Error']):
    print 'Last_SQL_Error: ' + str(show_status['Last_SQL_Error'])

if 'Using_Gtid' in show_status:
    print 'Using_Gtid: ' + str(show_status['Using_Gtid'])
print 'Seconds_Behind_Master: ' + str(show_status['Seconds_Behind_Master'])

sys.exit(0)
