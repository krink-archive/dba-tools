
conf="/etc/db.conf"
#define('dbaUser','user');
#define('dbaPass','pass');

dbaUser=$(grep ^define $conf | grep dbaUser | awk -F"," '{print $2}' | awk -F"'" '{print $2}')
dbaPass=$(grep ^define $conf | grep dbaPass | awk -F"," '{print $2}' | awk -F"'" '{print $2}')
dbaServer="localhost"

MYSQL_USER=$dbaUser
MYSQL_PASS=$dbaPass

MYSQL_CONN="-u${MYSQL_USER} -p${MYSQL_PASS}"

MYSQLDUMP_OPTIONS=" --master-data=2 --single-transaction --quick "
mysqldump ${MYSQL_CONN} ${MYSQLDUMP_OPTIONS} --all-databases   > mysqldump.all-databases.`date --iso`.sql

#https://dev.mysql.com/doc/refman/5.5/en/mysqldump.html
#--master-data=2
#If the option value is 2, the CHANGE MASTER TO statement is written as an SQL comment, and thus is informative only;
# it has no effect when the dump file is reloaded. If the option value is 1, 
# the statement is not written as a comment and takes effect when the dump file is reloaded.
# If no option value is specified, the default value is 1. 

