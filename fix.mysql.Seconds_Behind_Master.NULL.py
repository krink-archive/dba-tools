#!/bin/env python

__version__='00.2.a'

import os
import sys
import subprocess
import datetime
import re
import socket

#db.creds are necessary
#dbuser = 'xxxx'
#dbpass = 'xxxx'

dbconf  = '/etc/db.conf'

with open(dbconf) as conf:
  for line in conf:
    if line.startswith("define('dbaUser'"):
      dbuser = line.split(',')[1].strip('\'').split('\'')[0]
    if line.startswith("define('dbaPass'"):
      dbpass = line.split(',')[1].strip('\'').split('\'')[0]

config = {
  'user': dbuser,
  'password': dbpass,
  'unix_socket': '/var/lib/mysql/mysql.sock',
  'database': 'mysql',
  'raise_on_warnings': True,
}


#email.alerts
TO = ['karl@usaepay.com,server@usaepay.com']
SUBJECT = 'testing repair_table'
FROM = socket.gethostname() + '@usaepay.com'

logfile = 'repair_output.log'
debug=None

def sendMail(FROM,TO,SUBJECT,TEXT):
    import smtplib
    """this is some test documentation in the function"""
    message = """\
        From: %s
        To: %s
        Subject: %s
        %s
        """ % (FROM, ", ".join(TO), SUBJECT, TEXT)
    # Send the mail
    server = smtplib.SMTP('localhost')
    server.sendmail(FROM, TO, message)
    server.quit()

def saveLog(logfile,msg):
    now = str(datetime.datetime.now())
    print 'Save ' + now
    with open(logfile, 'a') as file:
        file.write(now + ' ' + msg + '\n')

def Main():
    # test with an input file, if you will
    if sys.argv[1:]:
        print 'One arg' + sys.argv[1]
        file = sys.argv[1]
        with open(file) as f:
            lines = f.readlines()
            for line in lines:
                if debug: print line.split()
                #print line
                if 'Seconds_Behind_Master' in line:
                    #print line.split()[1]
                    Seconds_Behind_Master = line.split()[1]
                if 'Last_SQL_Errno' in line:
                    #print line.split()[1]
                    Last_SQL_Errno = line.split()[1]
                if 'Last_SQL_Error' in line:
                    #print line.split()[1:]
                    Last_SQL_Error = line.split()[1:]

    else:
        sql = 'SHOW SLAVE STATUS \G;'
        cmdline = ["mysql", "-u", dbuser, "-p%s" % dbpass, "-e", sql ]
        p = subprocess.Popen(cmdline,stdout=subprocess.PIPE)
        retcode = p.wait()
        if (retcode != 0):
            print 'Error:  not working properly'
            saveLog(logfile, 'Fatal Error subprocess.Popen cmdline mysql SHOW SLAVE STATUS')
            sys.exit(1)
        stdout = p.stdout.readline
        for line in iter(stdout,''):
            if debug: print line.split()
            #print line
            if 'Seconds_Behind_Master' in line:
                #print line.split()[1]
                Seconds_Behind_Master = line.split()[1]
            if 'Last_SQL_Errno' in line:
               #print line.split()[1]
                Last_SQL_Errno = line.split()[1]
            if 'Last_SQL_Error' in line:
                #print line.split()[1:]
                Last_SQL_Error = line.split()[1:]

    #print 'Seconds_Behind_Master ' + Seconds_Behind_Master

    now = str(datetime.datetime.now())
    print now + ' ' + Seconds_Behind_Master

    repair_table=None

    if Seconds_Behind_Master == 'NULL':
        if debug: print 'NULL HIT!'
        print 'Last_SQL_Error ' + str(Last_SQL_Error)
        saveLog(logfile,str(Last_SQL_Error))
        list=Last_SQL_Error

        #m0260231.batches
        regex=re.compile("^m[0-9].*(batches).*")
        merchant_table = [m.group(0) for l in list for m in [regex.search(l)] if m]
        if debug: print 'merchant_table ' + str(type(merchant_table))
        try:
            repair_table = merchant_table[0]
            print 'match bathes'
        except IndexError:
            pass

        #m0146607.transactions
        regex=re.compile("^m[0-9].*(transactions).*")
        merchant_table = [m.group(0) for l in list for m in [regex.search(l)] if m]
        if debug: print 'merchant_table ' + str(type(merchant_table))
        try:
            repair_table = merchant_table[0]
            print 'match transactions'
        except IndexError:
            pass

    if debug: print type(repair_table)
    if repair_table:
        print 'repair_table'
        #mysql> repair table m0260231.batches; start slave;
        sql = 'repair table ' + repair_table + '; start slave;'
        print sql
        cmdline = ["mysql", "-u", dbuser, "-p%s" % dbpass, "-e", sql ]
        p = subprocess.Popen(cmdline,stdout=subprocess.PIPE)
        retcode = p.wait()
        if (retcode != 0):
            print 'Error:  not working properly'
            saveLog(logfile, 'Fatal Error subprocess.Popen cmdline mysql repair_table ' + sql)
            sys.exit(1)
        stdout = p.stdout.readline
        repair_output = []
        for line in iter(stdout,''):
            print line.split()
            repair_output.append(line)
            #if debug: print line.split()
            #print line 
        msg='repair_table ' + repair_table 
        msg += ', '.join(repair_output)
        sendMail(FROM,TO,SUBJECT,msg)
        saveLog(logfile,msg)

    sys.exit(0)

if __name__ == "__main__":
    Main()

print 'Exit'
sys.exit(0)
