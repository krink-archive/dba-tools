#!/bin/env python

__version__ = 'start.slave.0.0.1'

dbconf  = '/etc/db.conf'

with open(dbconf) as conf:
  for line in conf:
    if line.startswith("define('dbaUser'"):
      dbaUser = line.split(',')[1].strip('\'').split('\'')[0]
    if line.startswith("define('dbaPass'"):
      dbaPass = line.split(',')[1].strip('\'').split('\'')[0]

config = {
  'user': dbaUser,
  'password': dbaPass,
  'unix_socket': '/var/lib/mysql/mysql.sock',
  'database': 'mysql',
  'raise_on_warnings': True,
}

import sys
try:
  import mysql.connector
except ImportError as e:
  print e
  print 'yum install mysql-connector-python'
  sys.exit(1)

try:
  cnx = mysql.connector.connect(**config)
except mysql.connector.Error as e:
  print e
  sys.exit(1)

cursor = cnx.cursor(buffered=True)
try:
  sql = "select version();"
  cursor.execute(sql)
  select_version = cursor.fetchone()

  if str(select_version[0]).startswith("10"):
      sql = "show all slaves status;"
  else:
      sql = "show slave status;"
  cursor.execute(sql)
  if cursor.rowcount > 0:
      show_status = dict(zip(cursor.column_names, cursor.fetchone()))
  else:
      cursor.close()
      cnx.close()
      print 'No slave status'
      sys.exit(0)

  if 'Connection_name' in show_status:
      Connection_name = str(show_status['Connection_name'])

  if Connection_name:
      sql = "start slave '" + Connection_name + "';"
  else:
      sql = "start slave;"
  
  cursor.execute(sql)
  cnx.commit() 

except mysql.connector.Error as e:
  print e
  sys.exit(1)

cursor.close()
cnx.close()

#for key, value in show_status.iteritems():
#    print key, value

sys.exit(0)
