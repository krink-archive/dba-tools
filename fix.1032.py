#!/bin/env python

__version__='fix.1032.03'

import os
import sys
import subprocess
import datetime
import re
import socket

debug=False
email=True
logging=True
logfile = 'fix.1032.log'


#db.creds are necessary
#dbuser = 'xxxx'
#dbpass = 'xxxx'

dbconf  = '/etc/db.conf'

with open(dbconf) as conf:
  for line in conf:
    if line.startswith("define('dbaUser'"):
      dbuser = line.split(',')[1].strip('\'').split('\'')[0]
    if line.startswith("define('dbaPass'"):
      dbpass = line.split(',')[1].strip('\'').split('\'')[0]

config = {
  'user': dbuser,
  'password': dbpass,
  'unix_socket': '/var/lib/mysql/mysql.sock',
  'database': 'mysql',
  'raise_on_warnings': True,
}

this_hostname = socket.gethostname()
#email.alerts
TO = ['karl@usaepay.com','ops@usaepay.com']
SUBJECT = str(this_hostname) + ' fix.1032'
FROM = str(this_hostname) + '@usaepay.com'

def sendMail(FROM,TO,SUBJECT,TEXT):
    import smtplib
    """this is some test documentation in the function"""
    message = 'Subject: {}\n\n{}'.format(SUBJECT, TEXT)
    # Send the mail
    server = smtplib.SMTP('localhost')
    server.sendmail(FROM, TO, message)
    server.quit()

def saveLog(logfile,msg):
    now = str(datetime.datetime.now())
    print 'Save ' + now
    with open(logfile, 'a') as file:
        file.write(now + ' ' + msg + '\n')

def Main():
    Seconds_Behind_Master = ''
    Last_SQL_Errno = ''
    Last_SQL_Error = ''

    # test with an input file, if you will
    if sys.argv[1:]:
        print 'One arg' + sys.argv[1]
        file = sys.argv[1]
        with open(file) as f:
            lines = f.readlines()
            for line in lines:
                if debug: print line.split()
                #print line
                if 'Seconds_Behind_Master' in line:
                    if debug: print 'Seconds_Behind_Master' + str(line.split()[1])
                    Seconds_Behind_Master = line.split()[1]
                if 'Last_SQL_Errno' in line:
                    if debug: print 'Last_SQL_Errno' + str(line.split()[1])
                    Last_SQL_Errno = line.split()[1]
                if 'Last_SQL_Error' in line:
                    if debug: print 'Last_SQL_Error' + str(line.split()[1:])
                    Last_SQL_Error = line.split()[1:]

    else:
        #sql = 'SHOW SLAVE STATUS \G;'

        Connection_name = ''
        sql = 'SHOW ALL SLAVES STATUS \G;'
        cmdline = ["mysql", "-u", dbuser, "-p%s" % dbpass, "-e", sql ]
        p = subprocess.Popen(cmdline,stdout=subprocess.PIPE)
        retcode = p.wait()
        if (retcode != 0):
            print 'Error:  not working properly'
            if logging: saveLog(logfile, 'Fatal Error subprocess.Popen cmdline mysql SHOW SLAVE STATUS')
            sys.exit(1)
        stdout = p.stdout.readline
        for line in iter(stdout,''):
            if debug: print line.split()
            #print line
            if 'Seconds_Behind_Master' in line:
                if debug: print 'Seconds_Behind_Master' + str(line.split()[1])
                Seconds_Behind_Master = line.split()[1]
            if 'Last_SQL_Errno' in line:
                if debug: print 'Last_SQL_Errno' + str(line.split()[1])
                Last_SQL_Errno = line.split()[1]
            if 'Last_SQL_Error' in line:
                if debug: print 'Last_SQL_Error' + str(line.split()[1:])
                Last_SQL_Error = line.split()[1:]
            if 'Connection_name' in line:
                if debug: print 'Connection_name' + str(line.split()[1:])
                Connection_name = line.split()[1:]

    #print 'Seconds_Behind_Master ' + Seconds_Behind_Master

    now = str(datetime.datetime.now())
    print now + ' ' + Seconds_Behind_Master

##############################################################################
    if Seconds_Behind_Master == 'NULL':
        if debug: print 'NULL HIT!'
        print 'Last_SQL_Error ' + str(Last_SQL_Error)
        if logging: saveLog(logfile,str(Last_SQL_Error))
        list=Last_SQL_Error

##############################################################################
##############################################################################
    if Last_SQL_Errno == '1032':
        if debug: print '1032 ' + Last_SQL_Errno
        #mysql> SET GLOBAL sql_slave_skip_counter=1; start slave;

        #sql = "stop slave '" + str(Connection_name[0]) + "';SET GLOBAL sql_slave_skip_counter=1; start slave '" + str(Connection_name[0]) + "';"
        sql = "stop slave '" + str(Connection_name[0]) + "';SET @@default_master_connection='" + str(Connection_name[0]) + "';SET GLOBAL sql_slave_skip_counter=1; start slave '" + str(Connection_name[0]) + "';"
        print sql
        cmdline = ["mysql", "-u", dbuser, "-p%s" % dbpass, "-e", sql ]
        p = subprocess.Popen(cmdline,stdout=subprocess.PIPE)
        retcode = p.wait()
        if (retcode != 0):
            print 'Error:  not working properly'
            if logging: saveLog(logfile, 'Fatal Error subprocess.Popen cmdline mysql ' + sql)
            sys.exit(1)
        stdout = p.stdout.readline
        action_output = []
        for line in iter(stdout,''):
            print line.split()
            action_output.append(line)
            #if debug: print line.split()
            #print line
        msg='ER_DUP_ENTRY ' + str(Last_SQL_Errno) + ' sql_slave_skip_counter=1 '
        msg += ', '.join(action_output) + '\n\r\n'
        msg += str(Last_SQL_Error) + '\n\r\n'

        if email: sendMail(FROM,TO,SUBJECT,msg)
        if logging: saveLog(logfile,msg)

##############################################################################
    sys.exit(0)

if __name__ == "__main__":
    Main()

print 'Exit'
sys.exit(0)


